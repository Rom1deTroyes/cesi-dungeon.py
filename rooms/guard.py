#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Representation of a Dungeon Room
"""
from rooms.room import Room


class GuardRoom(Room):
    """
    The Guard Room
    """
    def __init__(self):
        Room.__init__(self)
        self.surname = "Guard Room !"
        self.description = "You see a dead corpse."

    def action(self):
        """
        Enter the room, and see what's happening !
        """
        raise Exception('Action', 'Ambush')
        return "It's a trap !"
