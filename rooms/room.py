#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Representation of a Dungeon Room

enum Room_Types {
    - Prison
    - Salle de gardes
    - Cuisinne
    - Chambre
    - Sortie
}

class Room {
    - Name
    - Type
    - Exits
}

class Exit_Room {
    - Name = "Exit"
    - action() : Victory !
}
Exit_Room --> Room

class Guard_Room {
    - Name : "Guard Room"
    - action() : Game Over !
}
Guard_Room --> Room

class Rest_Room {
    - Name : "Rest Room"
    - action() : gainHP
}
Rest_Room --> Room
"""


class Room:
    """
    The Room
    """
    surname = "Room"
    seen = False
    description = "You see an empty room."

    def __init_(self):
        self.surname = "Room One"

    def get_name(self):
        """Name getter"""
        return self.surname

    def set_name(self, new_name):
        """Name setter"""
        self.surname = new_name

    def is_known(self):
        """
        Tells if the Player has already enter this room (True) or not (False)
        """
        return self.seen

    def describe(self):
        """Description getter"""
        return self.description

    def enter(self):
        """
        Enter the room, and see what's happening !
        """
        dialog = "You enter " + self.get_name() + ". "

        if not self.seen:
            self.seen = True
            dialog += self.action()
        else:
            dialog += self.describe()
        return dialog

    def action(self):
        """
        What happens !
        """
        return " "
