#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Representation of a Dungeon Room
"""
from rooms.room import Room


class RestRoom(Room):
    """
    The Rest room
    """
    def __init__(self):
        Room.__init__(self)
        self.surname = "Rest Room"
        self.description = "You see an empty vial."

    def action(self):
        """
        Enter the room, and see what's happening !
        """
        raise Exception('Action', 'Medics')
        return "You found a red vial and drink what was inside..."
