#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Representation of a Dungeon Room
"""
from rooms.room import Room


class ExitRoom(Room):
    """
    The Exit
    """
    def __init__(self):
        Room.__init__(self)
        self.surname = "Exit"
        self.description = "You see the Light !"

    def action(self):
        """
        Enter the room, and see what's happening !
        @TODO : events.
        """
        self.seen = True
        raise Exception('GammeOver', 'Victory')
