#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Representation of a Dungeon Room
"""
from rooms.room import Room


class PrisonRoom(Room):
    """
    The Prison
    """
    def __init__(self):
        Room.__init__(self)
        self.surname = "Prison"
        self.description = "You see a well cleaned jail..."

    def action(self):
        """
        Enter the room, and see what's happening !
        """
        return "You start your journey in Prison... Good luck !"
