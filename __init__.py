# License: GNU GPL version 3, see the file "AUTHORS" for details.

import os


# Version helper
def version_helper():
    if __release__:
        version_string = 'dugeon {0}'.format(__version__)
    else:
        import subprocess
        version_string = 'dungeon-master {0}'
        try:
            git_describe = subprocess.Popen(['git', 'describe'],
                                            universal_newlines=True,
                                            cwd=DUNGEONDIR,
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
            (git_description, _) = git_describe.communicate()
            version_string = version_string.format(git_description.strip('\n'))
        except (OSError, subprocess.CalledProcessError):
            version_string = version_string.format(__version__)
    return version_string


# Information
__license__ = 'GPL3'
__version__ = '0.3.3'
__release__ = False
__author__ = __maintainer__ = 'Romain Heller'
__email__ = 'Rom1deTroyes@user.gitlab.com'

# Constants
DUNGEONDIR = os.path.dirname(__file__)
USAGE = '%prog [options] [path]'
VERSION = version_helper()

# These variables are ignored if the corresponding
# XDG environment variable is non-empty and absolute
CACHEDIR = os.path.expanduser('~/.cache/dungeon')
CONFDIR = os.path.expanduser('~/.config/dungeon')
DATADIR = os.path.expanduser('~/.local/share/dungeon')

args = None  # pylint: disable=invalid-name

from main import main  # NOQA pylint: disable=wrong-import-position
