#!/usr/bin/python -O
# License: GNU GPL version 3, see the file "AUTHORS" for details.

# =====================
# This embedded bash script can be executed by sourcing this file.

import sys

# Start ranger
import main  # NOQA pylint: disable=import-self,wrong-import-position
sys.exit(main.main())  # pylint: disable=no-member
