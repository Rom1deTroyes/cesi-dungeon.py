#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import randrange
from player import Player
from rooms.room import Room
from rooms.prison import PrisonRoom
from rooms.exit import ExitRoom
from rooms.guard import GuardRoom
from rooms.rest import RestRoom


def print_board(player):
    print(player.get_name(), " (", player.get_xp(), ") ", " : ",
          player.get_hp())

def main():

    player1 = Player()
    player2 = Player()

    player1.set_name("Romain")

    room0 = PrisonRoom()
    room1 = Room()
    room2 = GuardRoom()
    room3 = RestRoom()
    room4 = ExitRoom()

    rooms = [room1, room2, room3]

    castle = [room1 for i in range(10)]
    #castle = [
    #    room1, room1, room1, room1, room1, room1, room1, room1, room1, room1, room1
    #]

    for r in range(1, len(castle) - 1):
        castle[r] = rooms[randrange(len(rooms) - 1)]

    castle[0] = room0
    castle[len(castle) - 1] = room4

    for r in range(len(castle)):
        room = castle[r]
        print(room.surname)

    print(room0.enter())

    while True:
        try:
            player1.gain_xp(1)
            room = castle[randrange(len(castle))]
            print(room.enter())
            print_board(player1)

        except Exception as ex:
            cause, state = ex.args

            if cause == 'GammeOver':
                if state == 'Death':
                    print("Oops... You're dead !")
                elif state == 'Victory':
                    print("Victory : You're still alive !")
                break
            elif cause == 'Action':
                if state == "Ambush":
                    player1.loose_hp(randrange(1, 10))
                elif state == "Medics":
                    player1.gain_hp(1)

    print("Gamme Over !")
