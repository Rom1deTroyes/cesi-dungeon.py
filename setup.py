#!/usr/bin/env python
# License: GNU GPL version 3, see the file "AUTHORS" for details.

import dungeon


def main():
    setup(
        name='cesi-dungeon',
        description='A Python dungeon... Almost.',
        long_description=cesi - dungeon.__doc__,
        version=cesi - dungeon.__version__,
        author=cesi - dungeon.__author__,
        author_email=cesi - dungeon.__email__,
        license=cesi - dungeon.__license__,
        url='https://cesi-dungeon.github.io',
        keywords='dungeon',
        classifiers=[
            'Environment :: Console',
            'Environment :: Console :: Curses',
            'Intended Audience :: Developers',
            'Intended Audience :: End Users/Desktop',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Operating System :: MacOS',
            'Operating System :: POSIX',
            'Operating System :: Unix',
            'Programming Language :: Python :: 3.7',
            'Topic :: Desktop Environment :: Gammes',
            'Topic :: Gammes',
        ],
        cmdclass={'install_lib': InstallLib},
        scripts=scripts_hack(('cesi-dungeon.py', 'cesi-dungeon'), ),
        data_files=[
            ('share/applications', [
                'doc/cesi-dungeon.desktop',
            ]),
            ('share/man/man1', [
                'doc/cesi-dungeon.1',
            ]),
            ('share/doc/cesi-dungeon', [
                'doc/colorschemes.md',
                'CHANGELOG.md',
                'HACKING.md',
                'README.md',
            ]),
            ('share/doc/cesi-dungeon/config', findall('doc/config')),
            ('share/doc/cesi-dungeon/config/colorschemes',
             findall('doc/config/colorschemes')),
            ('share/doc/cesi-dungeon/examples', findall('examples')),
            ('share/doc/cesi-dungeon/tools', findall('doc/tools')),
        ],
        package_data={
            'cesi-dungeon': [
                'data/*',
                'config/rc.conf',
            ],
        },
        packages=(
            'cesi-dungeon',
            'cesi-dungeon.rooms',
        ),
    )


if __name__ == '__main__':
    main()
