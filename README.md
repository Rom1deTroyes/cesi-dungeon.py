# Programmation orientée objet avancée

Nous allons créer une nouvelle application.
Le but est de créer un jeu d’aventure simpliste dont voici le gameplay.

- Vous êtes un personnage qui se déplace dans un chateau, de pièce en pièce.
- Le but du jeu est de sortir du chateau.
- Vous démarrez le jeu dans une prison.
- Chaque pièce peut être lié à une pièce ou plusieurs pièces.
- Chaque pièce peut éventuellement comporté une sortie et aura un nom et un type (prison, salle de garde, cuisine, chambre etc.)
- Votre personnage peut se déplacer de pièce en pièce librement.
- Il possède des points de vie.
- Le jeu se déroule tour par tour et à chaque fois il peut se déplacer d’une pièce à une autre.
- Quand il se trouve dans une pièce, il y a un événement aléatoire ou pas parmis les suivant :
  - il ne se passe rien,
  - il se fait attaquer (et perd un certain nombre de point)
  - il trouve une fiole de vie qui lui recrédite des points de vie.
- Le joueur gagne quand il sort du chateau
- et il perd quand il n’a plus de point de vie.

Sur cette base de gameplay, on peut imaginer complexifier le jeu avec notamment l’ajout d’objet et de leur utilisation (par exemple: combattre, inspecter une pièce etc.).
- On peut imaginer croiser d’autres personnages (allié ou ennemi etc.).
- On peut aussi imaginer le calcul d’un Score basé sur le nombre de déplacement et le nombre de points de vie restant du joueur.
Mais avant celà, veillez à obtenir une première version fonctionnelle.

Pour parvenir à implémenter ce jeu. Vous devrez créer une classe Personnage et une classe Pièce.

Vous utiliserez l’héritage pour modéliser les différents types de pièces.
Vous êtes libre de créer d’autres classes si vous le souhaiter (Chateau, Evenement…).

La boucle principale de votre jeu pourra être une boucle while qui demande à chaque itération quelle action réaliser, avec une liste de choix.

 Vous utiliserez au maximum ce qui a été vu jusqu’à présent:
- listes,
- tuples,
- dictionnaires,
- exceptions…

Vous pouvez vous appuyer sur la bibliothèque standard dont le module math pour les nombres aléatoire.
