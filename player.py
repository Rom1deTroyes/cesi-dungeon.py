#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Representation of a Dungeon Player
"""


class Player:
    """
    The Player
    """

    surname = "Slayer"
    health = 20
    experience = 0

    def __init_(self, surname="Player"):
        self.surname = surname

    def get_name(self):
        """Name getter"""
        return self.surname

    def set_name(self, new_name):
        """Name setter"""
        self.surname = new_name

    def get_xp(self):
        """XP getter"""
        return self.experience

    def gain_xp(self, xp_amount):
        """
        Increase Experience
        """
        self.experience += xp_amount

    def get_hp(self):
        """HP getter"""
        return self.health

    def gain_hp(self, hp_amount):
        """
        Increase Health
        """
        self.health += hp_amount

    def loose_hp(self, hp_amount):
        """
        Decrease Health
        """
        self.health -= hp_amount
        if not self.is_alive():
            raise Exception('GammeOver', 'Death')

    def is_alive(self):
        """
        Tells if the Player is dead (False) or Alive (True)
        """
        alive = False
        if self.health > 0:
            alive = True
        return alive

    def hello(self):
        """Hello, World !"""
        return "Hello, I'm " + self.surname + " !"
