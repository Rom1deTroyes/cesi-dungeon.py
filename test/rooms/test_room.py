#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test a Room
"""

import pytest
from rooms.room import Room


class TestRoom(Room):
    def test_get_name(self):
        """Name getter"""
        assert self.get_name() == self.surname

    def test_set_name(self):
        """Name setter"""
        val = "Test Room"
        self.set_name(val)
        assert self.surname == val

    def test_is_known(self):
        """
        Tells if the Player has already enter this room (True) or not (False)
        """
        assert self.is_known() == self.seen

    def test_describe(self):
        """Description getter"""
        assert self.describe() == self.description

    def test_enter(self):
        """
        Enter the room, and see what's happening !
        """
        dialog = "You enter " + self.surname + ". "
        dialog_unseen = dialog + self.action()
        dialog_seen = dialog + self.describe()
        assert dialog_unseen == dialog + " "
        assert dialog_seen == dialog + self.description

    def test_action(self):
        """
        What happens !
        """
        assert self.action() == " "
