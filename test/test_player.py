#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test a Player
"""
import pytest
from player import Player


class TestPlayer(Player):
    """
    The Player
    """
    def test_get_name(self):
        """Name getter"""
        assert self.get_name() == self.surname

    def test_set_name(self):
        """Name setter"""
        val = "Test Player"
        self.set_name(val)
        assert self.surname == val

    def test_get_xp(self):
        """XP getter"""
        assert self.get_xp() == self.experience

    def test_gain_xp(self):
        """
        Increase Experience
        """
        xp_base = self.experience

        test_xp = [0, 1, 10, -1, -10, 19, 20, 100]

        for xp_amount in test_xp:
            self.gain_xp(xp_amount)
            assert self.experience == xp_base + xp_amount
            self.experience = xp_base

    def test_get_hp(self):
        """HP getter"""
        assert self.get_hp() == self.health

    def test_gain_hp(self):
        """
        Increase Health
        """
        hp_base = self.health

        test_hp = [0, 1, 10, -1, -10, 19, 20, 100]

        for hp_amount in test_hp:
            self.gain_hp(hp_amount)
            assert self.health == hp_base + hp_amount
            self.health = hp_base

    def test_loose_hp(self):
        """
        Decrease Health
        """
        hp_base = self.health
        test_hp = [0, 1, 10, -1, -10, 19]

        for hp_amount in test_hp:
            self.loose_hp(hp_amount)
            assert self.health == hp_base - hp_amount
            self.health = hp_base

    def test_loose_hp_raises(self):
        """
        Decrease Health to Death must raise an Exception
        """
        hp_base = self.health
        test_hp = [20, 100]

        for hp_amount in test_hp:
            with pytest.raises(Exception) as execinfo:
                self.loose_hp(hp_amount)
            assert execinfo.value.args[0] == 'GammeOver'
            assert execinfo.value.args[1] == 'Death'
            assert self.health == hp_base - hp_amount
            self.health = hp_base

    def test_is_alive(self):
        """
        Tells if the Player is dead (False) or Alive (True)
        """
        assert self.is_alive() == True
        self.health = 0
        assert self.is_alive() == False
        self.health = -10
        assert self.is_alive() == False

    def test_hello(self):
        """Hello, World !"""
        assert self.hello() == "Hello, I'm " + self.surname + " !"
